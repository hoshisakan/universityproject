import secrets
import string
import datetime
import sys
from database_connection import DataBase as DB
import os
import traceback

count = 0

def HandleError(exp_object=None):
    error_class = exp_object.__class__.__name__
    # TODO 例外類型
    detail = exp_object.args[0]
    # TODO 引發例外原因
    cl, exc, tb = sys.exc_info()
    lastCallStack = traceback.extract_tb(tb)[-1]
    fileName = lastCallStack[0]
    lineNumber = lastCallStack[1]
    funcName = lastCallStack[2]
    errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(
        fileName, lineNumber, funcName, error_class, detail)
    print(errMsg)


def createProductCode():
    # TODO 提取所有大小寫字母與數字
    alphabet = string.ascii_letters + string.digits

    # TODO 產生固定長度的隨機碼，目前預設長度為6字元
    while True:
        password = ''.join(secrets.choice(alphabet) for i in range(6))
        # TODO 如果有小寫字母在隨機產生的代碼中，返回True；反之，若皆為空沒有任何的小寫字母存在，則返回False
        if (any(c.islower() for c in password)
                # TODO 產生2個大寫字母
                and sum(c.isupper() for c in password) > 1
                # TODO 產生2個數字
                and sum(c.isdigit() for c in password) > 1):
            break
    return password

#TODO 取得儲存資料的時間
exc_time = lambda:datetime.datetime.now().strftime("%Y-%m-%d-%H_%M_%S")

#TODO 檢查生成的產品註冊碼是否已經存在於資料表，有則返回True；反之，則返回False
def checkProductCodeDuplicate(check_code=None):
    try:
        with DB() as db:
            sql_command = "SELECT * FROM User_data"
            db.execute(sql_command)
            data = db.fetchall()
            search_key = 'user_box_identify_code'

            for check in data:
                if check[search_key] == check_code:
                    return True
            return False
    except Exception as e:
        HandleError(e)

#TODO 插入生成的辨識碼至資料表中
def insertionProductCode(params=None):
    global count
    try:
        for times in range(0,params):
            create_product_code = createProductCode()
			#TODO 若生成的產品註冊碼未重複，則將其寫入至使用者的資料表中
            if checkProductCodeDuplicate(create_product_code) is False and create_product_code is not None:
                print(create_product_code)
                with DB() as db:
                    sql_command = "INSERT INTO User_data(user_box_identify_code) VALUES ('%s')" % create_product_code
                    db.execute(sql_command)
                    print(exc_time(), "成功儲存第", count + 1, "筆資料")
                    count += 1
            else:
                print('產品辨識碼:{}已經重複'.format(create_product_code))
    except Exception as e:
        HandleError(e)

if __name__ == '__main__':
    try:
        insertionProductCode(10)
    except Exception as e:
        HandleError(e)
    except KeyboardInterrupt:
        os._exit(0)
