from pymysql.cursors import DictCursor
#TODO 引入mysql模組
import pymysql
import sys
# TODO 引入配置檔的內容
import config as MySQL
import traceback

#TODO 連接資料庫的類別
class DataBase():
    #TODO 初始化資料庫所需要的參數並連線
    def __init__(self, *args):
        #TODO 開始連線
        self.connection = pymysql.connect(
            #TODO mysql資料庫主機位址
            host=MySQL.MYSQL_HOST,
            #TODO mysql資料庫使用者名稱
            user=MySQL.MYSQL_USER,
            #TODO mysql資料庫名稱
            database=MySQL.MYSQL_DB,
            #TODO mysql資料庫密碼
            password=MySQL.MYSQL_PASSWORD,
            #TODO mysql資料庫連接埠3306
            port=MySQL.MYSQL_PORT,
            #TODO 以字典形式返回資料
            cursorclass=DictCursor
        )
        #TODO 默認每個insert操作方式時會觸發的commit，避免頻繁的commit
        self.connection.autocommit(True)
        #TODO 產生一個指標，讓使用者能夠對資料庫進行操作
        self.cursor = self.connection.cursor()

    #TODO 只適用於with...as的語法，當__init__初始化執行完畢後，就會執行此方法
    def __enter__(self):
        return self.cursor

    #TODO 當SQL指令執行完後，會自動關閉指標與資料庫的連線
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cursor.close()
        self.connection.close()
