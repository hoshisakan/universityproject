from flask import jsonify,request,Blueprint,current_app
import traceback
import datetime
import sys
import os
from flask_bcrypt import Bcrypt
from tasks.module.database_connection import DataBase as DB
from tasks.module.validate_token import TokenCode

def GetStaticPath(dirname=None):
    static_file_path = os.path.abspath(os.path.join(os.getcwd(),'../../..'))
    full_path = os.path.join(static_file_path, dirname)
    return full_path

register = Blueprint('register', __name__, url_prefix='/register',
                  static_folder=GetStaticPath('frontend/dist/static'),
                  template_folder=GetStaticPath('frontend/dist'))

#TODO 初始化套件於當前的應用
bcrypt = Bcrypt(current_app)

update_data_count = 0

def HandleError(exp_object=None):
    error_class = exp_object.__class__.__name__
    # TODO 例外類型
    detail = exp_object.args[0]
    # TODO 引發例外原因
    cl, exc, tb = sys.exc_info()
    lastCallStack = traceback.extract_tb(tb)[-1]
    fileName = lastCallStack[0]
    lineNumber = lastCallStack[1]
    funcName = lastCallStack[2]
    errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(
            fileName, lineNumber, funcName, error_class, detail)
    print(errMsg)

exc_time = lambda:datetime.datetime.now().strftime("%Y-%m-%d-%H_%M_%S")

#TODO 若產品辨識碼正確，且使用者帳密與電郵沒有重複，則更新使用者的資料
def updateUserData(*args):
    global update_data_count
    try:
        # print(args[0],args[1],args[2],args[3])
        # print(type(args[0]))
        with DB() as db:
            sql_command = "UPDATE User_data SET user_name = '%s',user_password = '%s',user_email = '%s' \
                            WHERE user_box_identify_code = '%s'" % (args[1],args[2],args[3],args[0])
            exc_result = db.execute(sql_command)
            print(exc_time(), "成功更新第", update_data_count + 1, "筆資料")
            update_data_count += 1
        return exc_result
    except Exception as e:
        HandleError(e)

#TODO 檢查前端表單提交的資料是否與資料表中的數據重複，若無重複則更新數據；反之，返回False
def checkFieldData(**kwargs):
    try:
        index = 0
        with DB() as db:
            sql_command = "SELECT user_box_identify_code,user_name,user_password,user_email FROM User_data"
            db.execute(sql_command)
            data = db.fetchall()
            temp_check_result = {}
            response_check_result = {}
            #TODO 獲取正常欄位的數量，用於保存正常的欄位數量
            original_field_quantity = len(data[0]) - 1
            #TODO 獲取正常欄位的數量，用於計算異常欄位的數量
            normal_field_quantity = len(data[0]) - 1

            #TODO 密碼要略過不檢查
            for check in data:
                for tableFieldName,formFiledName in zip(check, kwargs):
                    if tableFieldName != 'user_password' or formFiledName != 'form_password':
                       if tableFieldName != 'user_box_identify_code' and check[tableFieldName] == kwargs[formFiledName]:
                           temp_check_result[formFiledName] = False
                       #TODO 如果當前檢查的欄位名稱並非是產品辨識且欄位輸入內容不存在於資料表中
                       elif index == 0 and tableFieldName != 'user_box_identify_code' and check[tableFieldName] != kwargs[formFiledName]:
                           temp_check_result[formFiledName] = True
                       #TODO 如果當前檢查的欄位名稱是產品辨識碼且欄位輸入內容存在於資料表中
                       elif tableFieldName == 'user_box_identify_code' and check[tableFieldName] == kwargs[formFiledName]:
                           temp_check_result[formFiledName] = True
                           if check['user_name'] is None and check['user_email'] is None and check['user_password'] is None:
                               temp_check_result['register_status'] = True
                           else:
                               temp_check_result['register_status'] = False
                       #TODO 如果當前檢查的欄位名稱是產品辨識碼且欄位輸入內容不存在於資料表中
                       elif index == 0 and tableFieldName == 'user_box_identify_code' and check[tableFieldName] != kwargs[formFiledName]:
                           temp_check_result[formFiledName] = False
                index += 1
            print(temp_check_result)

            #TODO 讀取檢查結果，若發現有重複的輸入，或者產品辨識碼不符合，則將其儲存至字典中，以及將正常欄位-1
            for key,value in temp_check_result.items():
              if key == 'register_status':
                if value is False:
                   normal_field_quantity = -1
              else:
                if value is False:
                   normal_field_quantity -= 1
              response_check_result[key] = value
            print('正常欄位數量:{}'.format(normal_field_quantity))

            if normal_field_quantity < original_field_quantity:
                for key,value in response_check_result.items():
                    if key == 'form_identity_code':
                        print('產品辨識碼是否存在(True表示存在)?:{}'.format(value))
                    elif key == 'register_status':
                        if key == 'register_status':
                            print('{}欄位的註冊狀態:(False表示已經註冊):{}'.format(key,value))
                        else:
                            print('{}註冊使用者其索引值:{}'.format(key,value))
                    else:
                        print('表單輸入{}是否重複?(False表示重複):{}'.format(key,value))
            else:
                #TODO 更新表單資料至資料表中
                print('準備更新欄位數據')
                update_result = updateUserData(
                    kwargs['form_identity_code'],
                    kwargs['form_username'],
                    password_encrypt(kwargs['form_password']),
                    kwargs['form_email']
                )
                print(update_result)
                #TODO 註冊資料更新成功
                if update_result > 0:
                    response_check_result['update_result'] = True
                else:
                    response_check_result['update_result'] = False
            return response_check_result
    except Exception as e:
        HandleError(e)

#TODO 接收表單的使用者註冊資料
@register.route('/user/confrim',methods=['POST'])
def register_confirm():
  if request.method == 'POST':
    username = request.get_json()['username']
    password = request.get_json()['password']
    email = request.get_json()['email']
    identity_code = request.get_json()['identity_code']
    check_result = checkFieldData(
      form_identity_code=identity_code,
      form_username=username,
      form_password=password,
      form_email=email
    )
    return jsonify(check_result)


#TODO 將輸入的密碼進行加密
def password_encrypt(form_password=None):
    if form_password is None:
        return None
        #TODO 若未收到使用者輸入的密碼，則返回None
    else:
        bcrypt_hash = bcrypt.generate_password_hash(form_password).decode('utf-8')
        return bcrypt_hash
        #TODO 回傳加密後的哈希碼
