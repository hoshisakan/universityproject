#TODO 從flask套件中引入Flask, jsonify, request, Blueprint的模組
from flask import jsonify, request, Blueprint
#TODO 引入json模組
import json
import threading
import sys
import os
import traceback
import base64
#TODO 引入時間的模組
import time
from datetime import datetime,timedelta
#TODO 引入異常檢測的模組
import tasks.mqtt.check_abnormal_data as CAD
#TODO 引入資料庫連線的模組
from tasks.module.database_connection import DataBase as DB
#TODO 引入取得檔案路徑的模組
from tasks.mqtt.file import GetFileWorkingPath as GP

#TODO 初始化與定義藍圖
detection = Blueprint('detection', __name__, url_prefix='/env')

#TODO 轉換資料庫dateime的日期格式
conver_date = lambda datetime_format_date: datetime.strftime(datetime_format_date,"%Y年%m月%d日 %H時%M分%S秒")

#TODO 從資料庫中先抓取偵測資料後，再以JSON的資料格式將資料進行回傳
@detection.route('/data', methods=['GET'])
def read_env_data():
    #TODO 連接資料庫
    with DB() as db:
        #TODO 從Sensor_data取出最新的一筆資料(先以ORDERY BY進行倒排，再以LIMIT限制資料筆數)
        sql_command = "SELECT * FROM Sensor_data ORDER BY d_created_date DESC LIMIT 1"
        #TODO 執行SQL指令
        db.execute(sql_command)
        #TODO 讀取資料並將其儲存至data的變數中
        data = db.fetchall()
        #TODO 儲存轉換後的資料
        storage_data = {}

        for read_data in data[0]:
            #TODO 如果讀取到的資料的鍵是d_date，則會執行以下程式
            if read_data == 'd_created_date':
                #TODO 轉換資料庫dateime的日期格式
                conver_date_result = conver_date(data[0][read_data])
                #TODO 將轉換後日期儲存至storage_data的變數中，字典的鍵為read_data讀取到的鍵
                storage_data[read_data] = conver_date_result
            #TODO 反之，直接將資料儲存至storage_data的變數中
            else:
                storage_data[read_data] = data[0][read_data]
    return jsonify(storage_data)

#TODO 先從資料庫抓取健康度數值與辨識日期的資料後，再以JSON的資料格式將資料進行回傳
@detection.route('/plant/health/data', methods=['GET'])
def read_plant_health():
    #TODO 連接資料庫
    with DB() as db:
        # TODO 從Sensor_data取出最新的一筆資料(先以ORDERY BY進行倒排，再以LIMIT限制資料筆數)
        sql_command = "SELECT health_status,data_created_date FROM Growing_data ORDER BY data_created_date DESC LIMIT 1"
        #TODO 執行SQL指令
        db.execute(sql_command)
        #TODO 讀取資料並將其儲存至data的變數中
        data = db.fetchall()
        #TODO 儲存轉換後的資料
        storage_plant_health_data = {}

        for read_data in data[0]:
          #TODO 如果讀取到的資料的鍵是data_created_date，則會執行以下程式
          if read_data == 'data_created_date':
            #TODO 轉換資料庫dateime的日期格式
            conver_date_result = conver_date(data[0][read_data])
            #TODO 將轉換後日期儲存至storage_plant_health_data的變數中，字典的鍵為read_data讀取到的鍵
            storage_plant_health_data[read_data] = conver_date_result
          #TODO 反之，直接將資料儲存至storage_plant_health_data的變數中
          else:
            storage_plant_health_data[read_data] = data[0][read_data]
    return jsonify(storage_plant_health_data)

#TODO 取得最新創建的檔案名稱
def get_image_name(image_path=None):
    if image_path is not None:
        #TODO 列出圖像路徑的所有檔案
        image_files_list = os.listdir(image_path)
        #TODO 將檔案依據最近訪問的時間進行排序
        image_files_list.sort(
            key=lambda filename: os.path.getctime(os.path.join(image_path,filename)))
        #TODO 從該列表中取出倒數第一的檔案名稱，也就是最新的檔案名稱，將其與路徑結合後回傳
        latest_filename = os.path.join(image_path, image_files_list[-1])
        return latest_filename
    return None

#TODO 取得圖像或圖像的最近訪問日期
def get_image_stream(dirname=None,mode=None):
    #TODO 取得圖片路徑，將指定的圖片目錄與當前路徑結合
    image_path = GP.GetFullPath(dirname='static/{}'.format(dirname),mode=False)
    #TODO 檢查路徑是否存在
    if os.path.exists(image_path):
        #TODO 若存在則先取得該路徑底下最近訪問的檔案名稱
        latest_image_filename = get_image_name(image_path)
        #TODO 取得圖片的完整路徑
        image_full_path = GP.GetFullPath(filename=latest_image_filename,dirname='static/{}'.format(dirname),mode=True)
        #TODO 如果該路徑指向的目標是個檔案
        if os.path.isfile(image_full_path):
            #TODO 則True是返回圖片
            if mode is True:
                image_stream = ''
                #TODO 獲取圖片
                with open(image_full_path, 'rb') as image:
                    #TODO 讀取圖片以bytes的資料格式返回
                    image_content = image.read()
                    #TODO 再將bytes轉換成base64的資料格式
                    image_stream = base64.b64encode(image_content)
                return image_stream
            #TODO False是返回檔案最近訪問的日期
            else:
                #TODO 取得檔案的最近訪問日期
                get_file_created_time = lambda path: os.path.getctime(path)
                #TODO 暫存最新檔案的最近訪問日期
                time_stamp = get_file_created_time(image_full_path)
                #TODO 轉換時間格式
                conver_date_result = datetime.fromtimestamp(time_stamp).strftime("%Y年%m月%d日 %H時%M分%S秒")
                #TODO 回傳轉換後的日期
                return conver_date_result
        return False
    return False

#TODO 指定今日存放圖片的目錄
get_target_directory = lambda dirname: os.path.join(dirname,datetime.strftime(datetime.now(),"%Y-%m-%d"))

#TODO 指定昨天存放圖片的目錄
get_other_directory = lambda dirname=None,days=1:os.path.join(dirname,datetime.strftime(datetime.now() - timedelta(days), '%Y-%m-%d'))

#TODO 透端該路由返回轉換過後的圖片
@detection.route('/data/image', methods=['POST'])
def read_local_image():
    if request.method == 'POST':
        mode = request.get_json()['mode']
        if isinstance(mode, bool):
            #TODO True是返回圖片流，False是返回檔案最近訪問的日期
            response_data = get_image_stream(get_target_directory('img'),mode)
            #TODO 如果找不到今天日期的目錄
            if response_data is False:
                #TODO 將搜尋目標改為昨天日期的目錄
                # yesterday_original_image = get_image_stream(get_other_directory('img',1),mode)
                # if yesterday_original_image is False:
                #     before_yesterday_original_image = get_image_stream(get_other_directory('img',2),mode)
                #     return before_yesterday_original_image
                # return yesterday_original_image
                for days in range(1,8):
                    response_data = get_image_stream(get_other_directory('img',days),mode)
                    if response_data is not False:
                        break
            return response_data
        else:
            return 'Empty'

#TODO 透端該路由返回轉換過後的圖片
@detection.route('/data/image/anatomy/plant', methods=['POST'])
def read_local_anatomy_plant_image():
    if request.method == 'POST':
        mode = request.get_json()['mode']
        if isinstance(mode, bool):
            #TODO True是返回圖片，False是返回檔案最近訪問的日期
            response_data = get_image_stream(get_target_directory('plant_anatomy'),mode)
            #TODO 如果找不到今天日期的目錄
            if response_data is False:
                #TODO 將搜尋目標改為昨天日期的目錄
                # yesterday_anatomy_image = get_image_stream(get_other_directory('plant_anatomy',1),mode)
                # if yesterday_anatomy_image is False:
                #     before_yesterday_anatomy_image = get_image_stream(get_other_directory('plant_anatomy',2),mode)
                #     return before_yesterday_anatomy_image
                # return yesterday_anatomy_image
                for days in range(1,8):
                    response_data = get_image_stream(get_other_directory('img',days),mode)
                    if response_data is not False:
                        break
            return response_data
        else:
            return 'Empty'

