import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
import VueGoogleCharts from 'vue-google-charts'

import routes from './router/routes'
import store from './store'

import GlobalComponents from './globalComponents'
import GlobalDirectives from './globalDirectives'
import MaterialDashboard from './material-dashboard'
import axios from 'axios'
import VueAxios from 'vue-axios'

const router = new VueRouter({
  mode: 'history',
  routes,
  linkExactActiveClass: 'nav-item active'
})

Vue.use(VueRouter)
Vue.use(MaterialDashboard)
Vue.use(GlobalComponents)
Vue.use(GlobalDirectives)
Vue.use(VueAxios, axios)
Vue.use(VueGoogleCharts)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
