#!/bin/bash

#如果腳本運行時發生錯誤，即是有非零退出的代碼，則終止腳本執行立即退出
set -e

#如果.env檔案未提供欲創立的使用者名稱或使用者密碼
if ( [ -z "${MOSQUITTO_USERNAME}" ] || [ -z "${MOSQUITTO_PASSWORD}" ] ); then
  echo "MOSQUITTO_USERNAME or MOSQUITTO_PASSWORD not defined"
  #非正常運行導致其退出程序
  exit 1
fi

# 創建Mqtt的使用者密碼檔案，用於存放使用者名稱與其密碼，密碼與使用者名稱都是由.env賦予
cd /mosquitto
touch passwordfile
mosquitto_passwd -b passwordfile $MOSQUITTO_USERNAME $MOSQUITTO_PASSWORD

#產生一個Mqtt的運行日誌檔案
touch "$MOSQUITTO_LOGFILENAME"
chmod 777 "$MOSQUITTO_LOGFILENAME"

#重新導向輸入的變數
exec "$@"
